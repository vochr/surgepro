---
title: "Impressum und Datenschutzinformationen"
description: "Impressum"
slug: "imprint"
keywords: ""
categories:
    - ""
    - ""
date: 2017-10-31T22:42:51-05:00
draft: false
---

Verantwortliche für diese Webseite
---
Albert-Ludwigs-Universität Freiburg\
Professur für Waldwachstum und Dendroökologie\
Fakultät für Umwelt und natürliche Ressourcen\
Tennenbacher Str. 4\
D-79106 Freiburg\
0761/203-3737\
info@surge-pro.eu

Datenschutzbeauftragter
---
Albert-Ludwigs-Universität Freiburg\
Der Datenschutzbeauftragte\
Fahnenbergplatz\
79085 Freiburg\
datenschutzbeauftragter@uni-freiburg.de

Verweis
---
Auf dieser Webseite werden keine weiteren Daten erfasst, als diejenigen
die unter der Webseite zur
[Datenschutzerklärung](https://uni-freiburg.de/universitaet/datenschutzerklaerung/)
der Universität Freiburg dargestellt.


Ihre Rechte
---
Sie haben das Recht, von der Universität Freiburg Auskunft über die zu
Ihrer Person gespeicherten Daten zu erhalten und/oder unrichtig
gespeicherte Daten berichtigen zu lassen.

Sie haben darüber hinaus das Recht auf Löschung oder auf Einschränkung
der Verarbeitung oder ein Widerspruchsrecht gegen die Verarbeitung.

Außerdem haben Sie in dem Fall, in dem Sie uns eine Einwilligung zur
Verarbeitung Ihrer personenbezogener Daten erteilt haben, das Recht,
die Einwilligung jederzeit zu widerrufen, wobei die Rechtmäßigkeit der
aufgrund der Einwilligung bis zum Widerruf erfolgten Verarbeitung nicht
berührt wird. Bitte wenden Sie sich dazu an datenschutz@uni-freiburg.de.
Sofern die Datenverarbeitung auf einer Einwilligung oder auf einem
Vertrag beruht, weisen wir Sie an der entsprechenden Stelle darauf hin.

Erfolgt die Datenverarbeitung zudem mithilfe automatisierter Verfahren,
steht Ihnen gegebenenfalls ein Recht auf Datenübertragbarkeit zu
(Art. 20 DS-GVO).

Sie haben das Recht auf Beschwerde bei einer Aufsichtsbehörde, wenn Sie
der Ansicht sind, dass die Verarbeitung der Sie betreffenden
personenbezogenen Daten gegen die Rechtsvorschriften verstößt. Eine
solche Aufsichtsbehörde ist beispielsweise der Landesbeauftragte für
den Datenschutz und die Informationsfreiheit Baden-Württemberg
