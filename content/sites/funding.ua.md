---
title: "Фінансування проєкту"
description: ""
slug: "funding"
image: funding_waterfall.jpg
keywords: ""
categories:
    - ""
    - ""
date: 2017-10-31T21:28:43-05:00
draft: false
---

![../../../logo/VWST_W.png](../../../logo/VWST_W.png)

SURGE-Pro, як і попередній проєкт SURGE, повністю фінансується за рахунок фундації
[VolkswagenStiftung](https://www.volkswagenstiftung.de/en) (Az: 97781).\
VolkswagenStiftung - це найбільша в Німеччині приватна неприбуткова організація, яка сприяє дослідницьким та освітнім проєктам, соціальним наукам та гуманізму. Це не є відділення групи компаній Volkswagen, це економічно незалежна та автономна організація, яка сама приймає рішення.
