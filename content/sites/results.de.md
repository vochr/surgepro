---
title: "Projektergebnisse"
description: ""
slug: "results"
image: results_outlook.jpg
keywords: ""
categories:
    - ""
    - ""
date: 2017-10-31T21:28:43-05:00
draft: false
---

Das SURGE-Pro Projekt startet am 01.02.2021 und läuft bis 30.01.2024.
An dieser Stelle werden verschiedene Projektergebnisse zur Verfügung
gestellt, wie z.B. Artikel, Präsentationen, Berichte und
Schulungsmaterialien.

## Artikel

## Konferenzbeiträge
[All-IUFRO Conference 2022](../../../docs/SURGE-Pro-IUFRO-Vienna-2022.pdf)

## verschiedenes
[Projektantrag](../../../docs/SURGE-Pro-project-description.pdf)\
[Jahresbericht 2021](../../../docs/SURGE-Pro-annual-report-2021.pdf)\
[Jahresbericht 2022](../../../docs/SURGE-Pro-annual-report-2022.pdf)

## SURGE (Vorstudie)
[Schlussbericht der Vorstudie SURGE.](../../../docs/SURGE-Final-report-draft.pdf)

## Projekttreffen
Im folgenden sind Materialien der verschiedenen Projekttreffen verfügbar:\
Kick-Off Meeting 10.03.2021: [Vorstellung des Projekts](../../../docs/20210310-Kick-off-projectdescription.pdf) und
[Arbeitspakete.](../../../docs/20210310-Kick-off-workingpackages.pdf)

## Gast-Seminar an der Universität Freiburg vom 20.07.2023
Weitere Informationen [hier](../seminar).
