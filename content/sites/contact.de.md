---
title: "Kontakt"
description: "Wer wir sind"
slug: "contact"
image: contact_waytogo.jpg
keywords: ""
categories:
    - ""
    - ""
date: 2017-10-31T22:26:09-05:00
draft: false
---
Haben Sie Fragen, Anregungen oder Interesse am Projekt?\
Wollen Sie eine Bachelor- oder Masterarbeit schreiben?

Melden Sie sich bei uns!

Albert-Ludwig-Universität Freiburg
---
Prof. Dr. Hans-Peter Kahle: Hans-Peter.Kahle@wwd.uni-freiburg.de\
Christian Vonderach: christian.vonderach@wwd.uni-freiburg.de

Forstliche Versuchs- und Forschungsanstalt Baden-Württemberg
---
Dr. Gerald Kändler: gerald.kaendler@forst.bwl.de

Ukrainian National Forest University
---
Prof. Dr. Vasyl Lavvny: v.lavnyy@nltu.edu.ua\
Associate Prof. Dr. Serhii Havryliuk: serhii.havryliuk@nltu.edu.ua\
Associate Prof. Dr. Mykola Korol: m.korol@nltu.edu.ua\
Associate Prof. Dr. Petro Khomiuk: khompetro@nltu.edu.ua\
Postgraduate student Oleksandr Matusevych

Lviv Polytechnic National University
---
Prof. Dr. Mykola Gusti: kgusti@yahoo.com
