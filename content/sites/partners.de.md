---
title: "Projektbeteiligte"
description: "ALU - UNFU - STSFTU - FVA"
slug: "partners"
image: partners_Herderbau.png
keywords: "ALU"
categories:
    - ""
    - ""
date: 2017-10-31T22:26:13-05:00
draft: false
---
Aktuell sind drei Forschungsinstitute aus zwei Ländern (Ukraine und Deutschland)
in diesem Projekt involviert.
Diese verbindet eine lange Geschichte der Zusammenarbeit auf institutioneller
aber auch persönlicher Ebene. Das Projekt startete mit einer kleinen
1-Jahres-Vorstudie **SURGE**. Das Ergebnis dieser Studie ist
[hier]({{< relref "results.md#SURGE" >}}) zu finden.

---

![../../../logo/IWW.jpg](../../../logo/IWW.jpg)\
[Professur für Waldwachstum und Dendroökologie](http://www.iww.uni-freiburg.de/index.html?set_language=de)\
[Fakultät for Umwelt und natürliche Ressourcen](https://www.unr.uni-freiburg.de/de)\
[Albert-Ludwig-Universität Freiburg](https://www.uni-freiburg.de)

---

![../../../logo/UNFU.jpg](../../../logo/UNFU.jpg)\
[Ukrainian National Forestry University (UNFU), Lviv, Ukraine](https://nltu.edu.ua/index.php/en/ )

---

<!-- ![../../../logo/UNFU.jpg](../../../logo/STSFTU.jpg)\
[St. Petersburg State Forest Technical University, St. Petersburg, Russia](http://en.spbftu.ru/) -->


![../../../logo/fva.jpg](../../../logo/fva.jpg)\
[Abteilung für Biometrie und Informatik](https://www.fva-bw.de/abteilungen/biometrie-informatik)\
[Forstliche Versuchs- und Forschungsanstalt Baden Württemberg](http://www.fva-bw.de)  

---

Zu Beginn des Projekts gab es eine vierte Partnerinstitution (St. Petersburg State Forest Technical University).
Diese ist seit dem 25.02.2022 aufgrund des russischen Angriffskriegs auf die
Ukraine von den Projektaufgaben suspendiert. Siehe auch die Einträge in den [News]({{< relref "news.md" >}}).
