---
title: "Project results"
description: ""
slug: "results"
image: results_outlook.jpg
keywords: ""
categories:
    - ""
    - ""
date: 2017-10-31T21:28:43-05:00
draft: false
---

The SURGE-Pro project just started on 1st February 2021. Later on,
you'll find downloadable documents like articles, presentations, reports
and other material on this page.

## Articles

## Conference contribution
[All-IUFRO Conference 2022](../../../docs/SURGE-Pro-IUFRO-Vienna-2022.pdf)

## miscellaneous
[Project proposal](../../../docs/SURGE-Pro-project-description.pdf)\
[Annual report 2021](../../../docs/SURGE-Pro-annual-report-2021.pdf)

## SURGE (preliminary study)
[Final report of the preliminary SURGE study.](../../docs/SURGE-Final-report-draft.pdf)

## Project meetings
Below, you can find material of different project meetings:\
Kick-Off Meeting 10.03.2021: [Introduction to the project](../../../docs/20210310-Kick-off-projectdescription.pdf) and
[working packages.](../../../docs/20210310-Kick-off-workingpackages.pdf)

## Gast-Seminar at the University of Freiburg, 20.07.2023
More Information [here](../seminar).
