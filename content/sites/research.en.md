---
title: "Research Program"
description: "This page contains the project description. blog4"
slug: "research"
image: researchplan.jpg
keywords: ""
categories:
    - ""
    - ""
date: 2017-10-31T22:42:51-05:00
draft: false
---

## preliminary remark
Due to the Russian war against Ukraine and the suspension of the Russian partner
according to the requirements of our funder, we had to adjust our project plan.
This page reflects the approved status as of summer 2022.\
The originally planned and approved version can be found
[here](../researchplanned).\
Date: 17.08.2022

## Aims
This project aims at a consistent assessment of the potential future role of
Norway spruce in the model regions. To achieve our goal, we hypothesize:

1. that current trends in growth and productivity, and in mortality and
regeneration of Norway spruce in Western Ukraine and Southwest Germany
are indicative of a **non-sustainable development**
2. that adjusted management practice can stabilise Norway spruce forests so
that they can continue to contribute to adaptation and mitigation of climate
change

We developed our research plan along three data streams: **forest
inventory data**, **forest growth data**, and **satellite data** (see
figure above). The three data streams are closely interlinked through
the forest growth simulator **EFISCEN**.

## Working packages (WP)

The project workload is split into several working packages. Besides an
administrative base working package, there are seven working packages
with different foci.

### WP 1: Forest inventory data  (completed January 2022)
Development of the common template for all project partners for data
collection and conducting of data collection which includes information
about Norway spruce forest for the agreed regions in Western Ukraine
(administrative units: Lviv, Ivano-Frankivs'k, Chernivtsi, Sakarpatska),
Northwest Russia (administrative units: Leningrad region, Novgorod
region, Pskov region) and Southwest Germany (administrative unit:
Baden-Württemberg) in respect of area, growing stock, productivity,
distribution of the area and growing stock over age classes (e.g.,
young, middle aged, premature, mature and over mature), increment (total
and mean), allowable and real cuttings as well as damaging factors
(incl. mortality). The official forest statistics will be used as the
main source of information to guarantee the data quality and respective
references. The collected data will serve as the data basis to implement
and run the EFISCEN model for scenario modeling of Norway spruce forest
development in the future.

This working package was completed in January 2022; there is no joint report or
publication due to the suspension of SPSFTU. A short summary of the results can
be found in the [Annual Report 2021](../results).

Contact: ALU

### WP 2: Growth data base
Selection of sample sites according to different age groups of Norway
spruce (age classes: less than 40 years; 41-80 years and more than 80).
Additionally, the sample sites will be stratified according to (i)
altitude, (ii) slope aspects, (iii) forest site types, (iv) types of
geographical positions, for all model regions in Western Ukraine. Increment
cores will be taken from sample trees. Tree-ring width will be measured on
increment cores using the existing measurement stations at partner institutions.

For the area of southwestern Germany, existing data are used, taken at different
locations in the Black Forest along an elevation gradient.

Growth data will be quality checked and cross-dated according to the principles
applied in dendroecological and growth and yield studies. The growth
data will be preprocessed to same conditions for the compilation of the
common growth database.

Contact: UNFU

### WP 3: Satellite data base
Collection of satellite images from different sensors (Landsat
and Sentinel-2) according to areas of interests of different regions of
investigation (Western Ukraine and Southwest Germany).
All available satellite data from the beginning of surveying (for
example for Landsat from the 1980s, Sentinel-2 from 2015) without cloud cover
(less than 10%) will be collected and harmonized.
For these images NDVI-indexes will be calculated for detecting relations with
growth data. For all three model regions the dynamics of coniferous wood
cover will be estimated in 5-year periods.

Contact: UNFU

### WP 4: GIS data base
The data obtained in WP2 on tree growth rates (“ground truth”), the data
obtained in WP3 on the NDVI (satellite) as well as supplementary data on
elevation, slope, exposition, soil properties, temperature and precipitation
will be combined in a GIS database for the agreed areas in Western Ukraine and
Southwest Germany. After analyzing the structures and formats of the Growth
database (WP2) and the Satellite database (WP3) we will select an optimal
structure and format for the GIS database for further joint analysis of the
growth rates and NDVI and environmental data.

The growth rate, the NDVI data and the environmental data will be used for
developing a statistical model of forest increment. The model will be used for
projecting of forest development for a set of future climate change scenarios
which will be agreed between the partners.

Contact: UNFU

### WP 5: Growth simulation
So far **EFISCEN** has not been used for the assessment of forest
development and timber supply based on National Forest Inventory (NFI)
data in Baden-Württemberg. The model will be run using the most recent
NFI data from 2012. This data as well as those from Western Ukraine and
Northwest Russia has to be prepared and specified according to the
requirements of EFISCEN. A major task will be to fit the growth
functions for Norway spruce. Different scenarios will be developed. The
results obtained for the
Baden-Württemberg region will be compared with the projections made by
the German WEHAM-model for validation. The results of the EFISCEN
results for the three regions will be analyzed and interpreted jointly.

Contact: FVA BW

### WP 6: Synthesis
Based on the results from the forest growth simulation, the
implications of the different scenarios on ecosystem services provided
by the forests, like provisioning and regulating services, but also
social and supporting services will be discussed. We will
use published functional relationships between ecosystem structures (as
provided by the scenario modelling) and ecosystem functions to
derive a synthesis assessment.

Contact: ALU

### WP 7: Outreach
The planned outreach activities aim at transferring the newly generated
information and scientific knowledge into forest-based land-use planning
in order to guide decision makers on strategic, tactical and operational
levels. This will be achieved through the active participation of actors
in the dissemination of project results, including
1. preparation of scientific material/publications based on the data and
ideas developed and obtained during the projects’ lifetime
2. development of teaching materials for the students on bachelor,
master and PhD levels, such as thematic modules in the courses on
sustainable forest management
3. using of project outcomes in refreshing teaching
4. reporting of the project results at scientific conferences, seminars,
workshops, summer schools etc. as well as presenting them in mass media
and for interested target groups such as forest authorities, forest
managers, representatives of forest business, NGOs in nature protection
etc., and
5. through the development of this project website and issue of thematic
leaflets.

Contact: UNFU
