---
title: "Projektförderung"
description: ""
slug: "funding"
image: funding_waterfall.jpg
keywords: ""
categories:
    - ""
    - ""
date: 2017-10-31T21:28:43-05:00
draft: false
---

![../../../logo/VWST_W.png](../../../logo/VWST_W.png)

Das Projekt SURGE-Pro, ebenso wie das Vorgängerprojekt SURGE, wird durch
 die [VolkswagenStiftung](https://www.volkswagenstiftung.de/en)
gefördert (Az: 97781).\
Die VolkswagenStiftung ist die größte deutsche private gemeinnützige
Organisation zur Förderung von Forschung und Lehre in Natur-, Geistes-
und Gesellschafts- sowie Ingenieurwissenschaften. Sie ist wirtschaftlich
autark und in ihren Entscheidungen autonom (vgl. Eintrag auf
[Wikipedia](https://de.wikipedia.org/wiki/Volkswagenstiftung)).
