---
title: "Contact"
description: "get in touch"
slug: "contact"
image: contact_waytogo.jpg
keywords: ""
categories:
    - ""
    - ""
date: 2017-10-31T22:26:09-05:00
draft: false
---
You got questions, suggestions or are interested in this project?\
You want to write a bachelor or master thesis?

Get in touch with us!

University of Freiburg
---
Prof. Dr. Hans-Peter Kahle: Hans-Peter.Kahle@wwd.uni-freiburg.de\
Christian Vonderach: christian.vonderach@wwd.uni-freiburg.de

Forest Reseach Institute Baden-Württemberg
---
Dr. Gerald Kändler: gerald.kaendler@forst.bwl.de

Ukrainian National Forest University
---
Prof. Dr. Vasyl Lavvny: v.lavnyy@nltu.edu.ua\
Associate Prof. Dr. Serhii Havryliuk: serhii.havryliuk@nltu.edu.ua\
Associate Prof. Dr. Mykola Korol: m.korol@nltu.edu.ua\
Associate Prof. Dr. Petro Khomiuk: khompetro@nltu.edu.ua\
Postgraduate student Oleksandr Matusevych

Lviv Polytechnic National University
---
Prof. Dr. Mykola Gusti: kgusti@yahoo.com
