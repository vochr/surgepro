---
title: "программа исследований (оригинал)"
description: "This page contains the project description. blog4"
slug: "researchplanned"
image: researchplan.jpg
keywords: ""
categories:
    - ""
    - ""
date: 2017-10-31T22:42:51-05:00
draft: false
---

**Эта страница устарела и относится к периоду с ыевраля 2021 года до
февраля 2022 года! Из-за войны России против Украины и приостановки
работы российского партнера нам пришлос скорректировать план нашего
проекта.\
С текущим статусом рабочего плана можно ознакомится
[здесь](../research)**.\
Date: 03.05.2021

## Цели
Данный проект направлен на последовательную оценку  роли  ели  обыкновенной в
трех модельных регионах. Для достижения этой цели были высказаны следующие
предположения:

1. современные тенденции в росте и продуктивности, а также в гибели и
восстановлении  ели обыкновенной  в Западной Украине и Юго-Западной Германии
свидетельствуют о **неустойчивом развитии**
2. рост и продуктивность ели обыкновенной  в бореальных лесах Северо-Запада
России может частично компенсировать потери в других регионах.

Был разработан план исследований на основе трех потоков данных: **данные
лесоустройства**, **данные о росте лесов** и **спутниковые данные**
(см. рисунок выше). Эти три потока данных тесно связаны между собой с помощью
 симулятора роста леса
**EFISCEN**.

## Рабочие пакеты (РП)
Объем работы по проекту разделен на несколько рабочих пакетов. Помимо
административного базового рабочего пакета, существует семь рабочих пакетов с
различной направленностью.

## РП 1: База данных лесной инвентаризации
Разработка общего для всех партнеров проекта шаблона для сбора данных и
проведение сбора данных, включающих информацию о произрастании ели обыкновенной
в согласованных регионах Западной Украины (административные единицы: Львовская,
Ивано-Франковская, Черновицкая, Закарпатская), Северо-Запада России
(административные единицы: Ленинградская область, Новгородская область,
Псковская область) и Юго-Запада Германии (административная единица:
Баден-Вюртемберг) в отношении площади, древостоя, продуктивности, распределения
площади и древостоя по классам возраста (например, молодняки, средневозрастные,
скороспелые, спелые и переспелые), прироста (общий и средний), допустимые и
реальные рубки, а также повреждающие факторы (включая гибель лесных насаждений).
Официальная лесная статистика будет использоваться в качестве основного
источника информации, чтобы гарантировать качество данных и соответствующие
ссылки. Собранные данные послужат информационной базой для внедрения и запуска
модели EFISCEN для сценарного моделирования развития лесов , в которых
произрастает ель обыкновенная, в будущем.

Контакт: СПБГЛТУ


## РП 2: База данных о росте
Отбор выборочных участков в соответствии с различными возрастными группами  ели
обыкновенной (классы возраста: менее 40 лет; 41-80 лет и более 80 лет). Кроме
того, выборочные участки будут стратифицированы по (i) высоте, (ii) склонам,
(iii) типам лесных участков, (iv) типам географического положения для всех
модельных регионов в Западной Украине, Северо-Западной России и Юго-Западной
Германии. С образцовых деревьев будут взяты керны прироста. Ширина древесных
колец будет измерена на кернах прироста с помощью существующих измерительных
приборов в партнерских учреждениях. Данные о росте будут проверены на качество и
перекрестно датированы в соответствии с принципами, применяемыми в
дендроэкологических исследованиях и исследованиях роста и урожайности. Данные о
росте будут предварительно обработаны до тех же условий для составления общей
базы данных о росте.

Контакт: УНЛУ

## РП 3: База спутниковых данных
Сбор спутниковых снимков с различных датчиков (MODIS, Landsat и Sentinel-2) в
соответствии с областями интересов различных регионов исследования (Западная
Украина, Северо-Запад России и Юго-Запад Германии). Будут собраны все доступные
спутниковые данные с начала съемок (например, для Landsat с 1980-х годов, MODIS
с 2000 года и Sentinel-2 с 2015 года) без облачности (менее 10%) в течение
5-летнего периода. Для этих изображений будут разработаны алгоритмы
предварительной обработки (атмосферные поправки, суммирование изображений и
т.д.) и база данных. Для изображений будут рассчитаны NDVI-индексы для выявления
связей с данными о росте. Для всех трех модельных регионов будет оценена
динамика хвойного лесного покрова за 5-летние периоды.

Контакт: УНЛУ

## РП 4: База данных ГИС
Разработка структуры специализированной многослойной базы данных ГИС с целью
объединения и анализа данных дистанционного зондирования (спутниковые снимки) и
наземных истинных (кольца деревьев) геопривязанных данных по ели обыкновенной
для согласованных территорий в Западной Украине, Северо-Западной России и
Юго-Западной Германии. Ретроспективный анализ роста деревьев  ели  обыкновенной
по градиенту Север-Юг для Северо-Запада России и по высоте над уровнем моря с
учетом температурного градиента для Западной Украины и Юго-Западной Германии.

Контакт: СПГЛТУ


## РП 5: Моделирование роста
До сих пор **EFISCEN** не использовался для оценки развития лесного хозяйства и
предложения древесины на основе данных Национального лесного кадастра (NFI) в
Баден - Вюртемберге. Модель будет запущена с использованием последних данных
NFI с 2012 год. Эти данные, а также данные по Западной Украине и Северо -
Западу России должны быть подготовлены и уточнены в соответствии с требованиями
EFISCEN. Основной задачей будет подгонка функций роста для  ели обыкновенной.
Будут разработаны различные сценарии. Результаты, полученные для региона
Баден-Вюртемберг, будут сравниваться с прогнозами, сделанными немецкой
EHAM-моделью для проверки. Результаты EFISCEN для трех регионов будут
проанализированы и интерпретированы совместно.

Контакт: Институт лесных исследований Баден Вюртемберг, Фрайбург

## РП 6: Синтез
На основе результатов моделирования роста лесов будут обсуждаться последствия
различных сценариев для экосистемных услуг, предоставляемых лесами, таких как
обеспечивающие и регулирующие услуги, а также социальные и поддерживающие
услуги. Мы будем использовать опубликованные функциональные связи между
структурами экосистем (как это предусмотрено моделированием сценариев) и
функциями экосистем для получения обобщающей оценки.

Контакт: Университет Альберта-Людвига во Фрайбурге

## РП 7: Информационная  деятельность
WP 7: Информационная  деятельность
Запланированная информационная  деятельность направлена на передачу вновь
полученной информации и научных знаний для планирование землепользования на
базе лесного хозяйства, чтобы помогать лицам, принимающим решения на
стратегическом, тактическом и оперативном уровнях. Это будет достигнуто путем
активного участия субъектов в распространении результатов проекта, включая
подготовку научных материалов/публикаций на основе данных и выводов,
разработанных и полученных в ходе реализации проекта:

разработка учебных материалов для студентов бакалавриата, магистратуры и
аспирантуры, таких как тематические модули, которые могут быть использованы  
в курсах по устойчивому управлению лесами;

использование результатов проекта в преподавании;

представление результатов проекта на научных конференциях, семинарах,
мастер-классах, летних школах и т.д., а также представление их в средствах
массовой информации и для заинтересованных целевых групп, таких как органы
управления лесами, руководители лесного хозяйства, представители лесного
бизнеса, неправительственные природоохранные организации и т.д., а также;

через развитие веб-сайта проекта и выпуск тематических листовок.

Контакты: УНЛУ и СПбГЛТУ
