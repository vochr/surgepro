---
title: "Imprint and Privacy Information"
description: "Imprint"
slug: "imprint"
keywords: ""
categories:
    - ""
    - ""
date: 2017-10-31T22:42:51-05:00
draft: false
---
Responsible institution for data protection purposes
---
University of Freiburg\
Chair of Forest Growth\
Faculty of Environment and Natural Resources\
Tennenbacher Str. 4\
D-79106 Freiburg\
++49 (0)761/203-3737\
info@surge-pro.eu

Data protection representative
---
University of Freiburg\
Data protection representative\
Fahnenbergplatz\
D-79085 Freiburg\
datenschutzbeauftragter@uni-freiburg.de

Statement
---
We do not collect further data on this webpage than stated on the
[data protection page](https://uni-freiburg.de/university/privacy-policy/)
of the University of Freiburg.


Your rights
---
You have the right to receive information from the University of
Freiburg on your personal data that has been retained and/or to have
retained data that is incorrect corrected.\
You also have the right to demand that your data be deleted or that the
processing thereof be restricted as well as to object to the processing
of your data.\
For more information, please contact us at datenschutz@uni-freiburg.de\
You have the right to file a complaint with the regulating authority if
you believe that the processing of your personal data is in violation of
the law. An example of such a regulating authority is
Baden-Württemberg’s state commissioner for data protection and freedom
of information.
