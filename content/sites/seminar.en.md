---
title: "Guest-Seminar"
description: "Seminar"
slug: "seminar"
keywords: ""
categories:
    - ""
    - ""
date: 2017-10-31T22:42:51-05:00
draft: false
---

After almost exactly 2.5 years of project duration, it was now possible to
invite the Ukrainian scientists to a project workshop in Freiburg. As part of
their research stay as guest scientists, they gave a seminar and presented their
work in the context of the project and beyond. The abstracts and slide sets can
be found on this page.\
Date: 21.07.2023

#### Prof. Dr. Vasyl Lavvny (UNFU)
#### Comparison of the growth of spruce stands in the Ukrainian Carpathians and in the Black Forest

The growth dynamics of spruce stands in the two predominant forest types in the
Ukrainian Carpathians will be described. It is shown that spruce stands of the
I and Ia site class grow approximately according to the productivity classes
(MAI) 10 and 12 of the yield tables used in Baden-Württemberg. In general,
the growth curves of spruce stands in the two regions are more similar with
respect to the development of stand height and stand diameter than of growing
stock and stand density.

[Folien (englisch)](../../../docs/LavnyySeminarUNR20230720.pdf)

#### Assoc. Prof. Dr. Serhii Havryliuk (UNFU)
##### Estimation of remotely sensed vegetation indices for spruce stands in the Ukrainian Carpathians and in the Black Forest

Remote sensing data nowadays are one of the main information sources on forests
on larger spatial scale; especially vegetation indexes (e.g. Normalized
Difference Vegetation Index, NDVI) are widely used. We extracted time series of
NDVI and Enhanced Vegetation Index (EVI) using Google Earth Engine for our
sample plots in Western Ukraine (Ukrainian Carpathians) and Baden-Württemberg
(Black Forest) for the time period of 1985 to 2022. Analyses of spatial and
temporal trends of Norway spruce dominated stands aims to better understand the
relation of these indices to climate and stand parameters in both countries.

[Folien (englisch)](../../../docs/HavryliukSeminarUNR20230720.pdf)

#### Prof. Dr. Mykola Gusti (UNFU, IASA)
##### Accounting for climate change in large-scale forest scenario models and use of tree-ring data

Large-scale forest models (e.g. EFISCEN, CBM, G4M) are suitable for the
assessment of forest dynamics for countries or regions using publicly available data. The models can be coupled to economic models (e.g. GLOBIOM) and energy models (e.g. PRIMES) for assessing the biomass demand impact on the forests. The models typically use average forest growth parameters, accounting for climate change is a challenge for them.

There are several ways of accounting for climate change in large-scale forest scenario models. Here we assess the response of the forests to climate change by analyzing tree-ring data collected from our sample plots in Western Ukraine and Southwestern Germany. Preliminary analysis of the tree-ring indices from the spruce stands in the Ukrainian Carpathians and in the Black Forest shows a significant positive response of the tree-ring indices to growing season precipitation and a negative response to air temperature. This information can be used for developing forest growth modifiers for large-scale forest scenario models.

[Folien (englisch)](../../../docs/GustiSeminarUNR20230720.pdf)

#### Dr. Mykola Korol (UNFU)
##### Productivity and state of health of spruce stands in the Ukrainian Carpathians.

The productivity of the spruce stands in the Ukrainian Carpathians is examined with regard to height above sea level and slope aspect. We assessed the state of health of the stands, by determining the state of health of each individual tree as a function of diameter. In addition, temporal variations and trends in mortality of spruce was investigated.

[Folien (deutsch)](../../../docs/KorolSeminarUNR20230720.pdf)
