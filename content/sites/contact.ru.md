---
title: "Контакты"
description: "get in touch"
slug: "contact"
image: contact_waytogo.jpg
keywords: ""
categories:
    - ""
    - ""
date: 2017-10-31T22:26:09-05:00
draft: false
---
У Вас есть вопросы, предложения или Вас заинтересовал проект?
Вы хотели бы работать над  выпускной квалификационной работой бакалавра или
магистерской диссертацией?

Свяжитесь с нами!

Университет Альберта-Людвига во Фрайбурге, Германия
---
Prof. Dr. Hans-Peter Kahle: Hans-Peter.Kahle@wwd.uni-freiburg.de\
Christian Vonderach: christian.vonderach@wwd.uni-freiburg.de

Институт лесных исследований Баден Вюртемберг, Фрайбург, Германия
---
Dr. Gerald Kändler: gerald.kaendler@forst.bwl.de

Национальный лесотехнический университет Украины, Львов
---
Профессор, д-р., Василий Лавный: v.lavnyy@nltu.edu.ua\
Доцент, д-р., Сергей Гаврилюк: serhii.havryliuk@nltu.edu.ua\
Доцент, д-р., Николай Король: m.korol@nltu.edu.ua\
Доцент, д-р., Петр Хомюк: khompetro@nltu.edu.ua\
Аспирант Александр Матусевич

Национальный университет "Львовская политехника"
---
Профессор, д-р., Николай Густи: kgusti@yahoo.com
