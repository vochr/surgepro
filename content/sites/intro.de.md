---
title: "Projekthintergrund"
description: ""
slug: "intro"
image: intro_smallspruce.jpg
keywords: ""
categories:
    - ""
    - ""
date: 2017-10-31T21:28:43-05:00
draft: false
---

In den letzten Jahrhunderten wurde in vielen Ländern Mittel-, Nord- und
Osteuropas bei der Wiederbewaldung und Waldrestaurierung die Rotfichte
(_Picea abies_ [L.] Karst.) bevorzugt. Die Fichte ist leicht zu etablieren
und zu bewirtschaften, hat schnelle Wachstumsraten und lässt hohe
wirtschaftliche Erträge erwarten. Heute wird das Verbreitungsgebiet
der von der Fichte dominierten Wälder weitgehend durch frühere
Bewirtschaftungspraktiken und nicht durch natürliche Faktoren bestimmt.
Während in Nordwestrussland der größte Teil der Fichte in ihrem
natürlichen Verbreitungsgebiet vorkommt, reicht sie in der Westukraine
und noch mehr in Südwestdeutschland weit über das angenommene
natürliche Verbreitungsgebiet hinaus. Aufgrund von Umweltveränderungen
und Änderungen der Bewirtschaftungsziele ändert sich die
Zusammensetzung der Waldbaumarten bereits heute. Diese Veränderungen
werden Auswirkungen auf die Holzproduktion sowie auf die
Kohlenstoffbindung, den Nährstoffkreislauf, die Artenvielfalt und die
Resistenz gegenüber abiotischen und biotischen Störungen haben. Die
damit verbundenen Veränderungen der Zusammensetzung, Struktur und
Funktion der Wälder wirken sich auf nahezu alle Produkte und
Dienstleistungen aus, einschließlich der sekundären Auswirkungen auf das
Einkommen der Waldbesitzer und auf das Klimaschutzpotenzial dieser
Wälder.

In Mittel- und Osteuropa ist die Fichte derzeit einer beispiellosen
Bedrohung durch mehrere abiotische und biotische Stressfaktoren
ausgesetzt. Die gegenwärtige Waldgesundheitskrise weist auf die geringe
Widerstandsfähigkeit und Anpassungsfähigkeit der Fichte an den
Klimawandel hin, und untermauert ihre Anfälligkeit gegenüber der
Klimaerwärmung. Dies gibt Anlass, die Größe des Klimaschutzpotenzials
der zukünftigen Wälder in Frage zu stellen.
Mittelfristige klimasensitive Szenarien sollen erstellt werden und
Prognosen zur Versorgung mit Fichtenholz unter besonderem Schwerpunkt
auf dem Übergang zu einer klimaneutralen, grünen Wirtschaftsweise
("green economy") liefern. Die erwarteten Ergebnisse werden einmalige und
zeitnahe Informationen über die künftige Verfügbarkeit von Fichtenholz
(insbesondere Stammholz) sein. Diese Informationen werden für den Forst- und
Holzsektor sowie für die politische Entwicklung im Hinblick auf den Übergang zu
Green Economies in Europa von hoher Relevanz sein.

### Anpassung der Projektziele

Aufgrund des russischen Angriffskriegs gegen die Ukraine mussten wir leider
unsere Projektziele anpassen. Durch die Suspension der russischen
Partnerinstitution konzentrieren wir uns zukünftig auf die verbleibenden
Partnerregionen (Westukraine und Südwestdeutschland). Im ersten Projektjahr
wurde zusätzlich das Projektgebiet Nordwestrussland betrachtet. Inhaltlich
ging es dabei um die frühere Entwicklung und den gegenwärtigen Zustand der
Fichte ([Arbeitspaket 1](../research)).

Die weiteren Projektergebnisse werden folglich keinen Vergleich zwischen der
zukünftigen Entwicklung der Wäldern der gemäßigten Breiten (Westukraine,
Südwestdeutschland) zur Entwicklung der borealen Wälder Nordwestrusslands ziehen
können. 

Stand: 16.08.2022
