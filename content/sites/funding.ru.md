---
title: "Финансирование проекта"
description: ""
slug: "funding"
image: funding_waterfall.jpg
keywords: ""
categories:
    - ""
    - ""
date: 2017-10-31T21:28:43-05:00
draft: false
---

![../../../logo/VWST_W.png](../../../logo/VWST_W.png)

SURGE-Pro, как и его предшественник SURGE, финансируется  Фольксваген Фонд
[VolkswagenStiftung](https://www.volkswagenstiftung.de/en) (Az: 97781).\
VolkswagenStiftung - это крупнейшая немецкая частная некоммерческая организация
по содействию исследованиям и образованию в области естественных, социальных и
гуманитарных наук. Он не связан с Volkswagen Group, экономически независим и
автономен в своих решениях.
