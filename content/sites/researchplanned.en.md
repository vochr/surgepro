---
title: "(originally planned) Research Program"
description: "This page contains the project description. blog4"
slug: "researchplanned"
image: researchplan.jpg
keywords: ""
categories:
    - ""
    - ""
date: 2017-10-31T22:42:51-05:00
draft: false
---

**This page is outdated and refers to the period of February 2021 to
February 2022! Due to the Russian war against Ukraine and the suspension of the
Russian partner, we had to adjust our project plan.\
The current status of the work plan can be found [here](../research)**.\
Date: 03.05.2021

## Aims
This project aims at a consistent assessment of the potential future role of
Norway spruce in the three model regions. To achieve our goal, we hypothesize:

1. that current trends in growth and productivity, and in mortality and
regeneration of Norway spruce in Western Ukraine and Southwest Germany
are indicative of a **non-sustainable development**
2. that gains in Norway spruce growth and productivity in the boreal
forests of Northwest Russia can partially compensate for losses in the
other regions.

We developed our research plan along three data streams: **forest
inventory data**, **forest growth data**, and **satellite data** (see
figure above). The three data streams are closely interlinked through
the forest growth simulator **EFISCEN**.

## Working packages (WP)

The project workload is split into several working packages. Besides an
administrative base working package, there are seven working packages
with different foci.

### WP 1: Forest inventory data base
Development of the common template for all project partners for data
collection and conducting of data collection which includes information
about Norway spruce forest for the agreed regions in Western Ukraine
(administrative units: Lviv, Ivano-Frankivs'k, Chernivtsi, Sakarpatska),
Northwest Russia (administrative units: Leningrad region, Novgorod
region, Pskov region) and Southwest Germany (administrative unit:
Baden-Württemberg) in respect of area, growing stock, productivity,
distribution of the area and growing stock over age classes (e.g.,
young, middle aged, premature, mature and over mature), increment (total
and mean), allowable and real cuttings as well as damaging factors
(incl. mortality). The official forest statistics will be used as the
main source of information to guarantee the data quality and respective
references. The collected data will serve as the data basis to implement
and run the EFISCEN model for scenario modeling of Norway spruce forest
development in the future.

Contact: SPSFTU

### WP 2: Growth data base
Selection of sample sites according to different age groups of Norway
spruce (age classes: less than 40 years; 41-80 years and more than 80).
Additionally, the sample sites will be stratified according to (i)
altitude, (ii) slope aspects, (iii) forest site types, (iv) types of
geographical positions, for all model regions in Western Ukraine,
Northwest Russia and Southwest Germany. Increment cores will be taken
from sample trees. Tree-ring width will be measured on increment cores
using the existing measurement stations at partner institutions. Growth
data will be quality checked and cross-dated according to the principles
applied in dendroecological and growth and yield studies. The growth
data will be preprocessed to same conditions for the compilation of the
common growth database.

Contact: UNFU

### WP 3: Satellite data base
Collection of satellite images from different sensors (MODIS, Landsat
and Sentinel-2) according to areas of interests of different regions of
investigation (Western Ukraine, Northwest Russia and Southwest Germany).
All available satellite data from the beginning of surveying (for
example for Landsat from the 1980s, MODIS from 2000 and Sentinel-2 from
2015) without cloud cover (less than 10%) within 5-years periods will be
collected. For these images pre-processing algorithms (atmospheric
corrections, stacking images, etc.) and database will be developed. For
the images NDVI-indexes will be calculated for detecting relations with
growth data. For all three model regions the dynamics of coniferous wood
cover will be estimated in 5-year periods.

Contact: UNFU

### WP 4: GIS data base
Development of the structure of specialized multilayer GIS database with
the aim to combine and analyze the remote sensing (satellite images) and
ground true (tree rings) georeferenced data on Norway spruce for the
agreed areas in Western Ukraine, Northwest Russia and Southwest Germany.
Retrospective analysis of Norway spruce tree growth on North-South
gradient for Northwest Russia and elevation above sea level incl.
temperature gradient for Western Ukraine and Southwest Germany.

Contact: SPSFTU

### WP 5: Growth simulation
So far **EFISCEN** has not been used for the assessment of forest
development and timber supply based on National Forest Inventory (NFI)
data in Baden-Württemberg. The model will be run using the most recent
NFI data from 2012. This data as well as those from Western Ukraine and
Northwest Russia has to be prepared and specified according to the
requirements of EFISCEN. A major task will be to fit the growth
functions for Norway spruce. Different scenarios will be developed. The
results obtained for the
Baden-Württemberg region will be compared with the projections made by
the German WEHAM-model for validation. The results of the EFISCEN
results for the three regions will be analyzed and interpreted jointly.

Contact: FVA BW

### WP 6: Synthesis
Based on the results from the forest growth simulation, the
implications of the different scenarios on ecosystem services provided
by the forests, like provisioning and regulating services, but also
social and supporting services will be discussed. We will
use published functional relationships between ecosystem structures (as
provided by the scenario modelling) and ecosystem functions to
derive a synthesis assessment.

Contact: ALU

### WP 7: Outreach
The planned outreach activities aim at transferring the newly generated
information and scientific knowledge into forest-based land-use planning
in order to guide decision makers on strategic, tactical and operational
levels. This will be achieved through the active participation of actors
in the dissemination of project results, including
1. preparation of scientific material/publications based on the data and
ideas developed and obtained during the projects’ lifetime
2. development of teaching materials for the students on bachelor,
master and PhD levels, such as thematic modules in the courses on
sustainable forest management
3. using of project outcomes in refreshing teaching
4. reporting of the project results at scientific conferences, seminars,
workshops, summer schools etc. as well as present-ing them in mass media
and for interested target groups such as forest authorities, forest
managers, representatives of forest business, NGOs in nature protection
etc., and
5. through the development of this project website and issue of thematic
leaflets.

Contact: UNFU and SPSFTU
