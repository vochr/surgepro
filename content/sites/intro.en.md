---
title: "Introduction to the project"
description: ""
slug: "intro"
image: intro_smallspruce.jpg
keywords: ""
categories:
    - ""
    - ""
date: 2017-10-31T21:28:43-05:00
draft: false
---

During recent centuries Norway spruce (_Picea abies_ [L.] Karst.) was
often favored in forest restoration activities in many Central, Northern
 and Eastern European countries; it was easy to establish and manage,
 had fast growth rates and was expected to provide high economic
 returns. Today, the range of Norway spruce dominated forests is largely
 determined by former management practices rather than by natural
 factors. While in Northwest Russia most of Norway spruce occurs within
 its natural range, in Western Ukraine and even more in Southwest
 Germany it reaches far beyond its assumed range. Due to environmental
 changes and changes in management aims, forest tree species composition
is already changing. These changes will have an impact on wood
production as well as on carbon sequestration, nutrient cycling,
biodiversity, and resistance against storm, snow, drought, fires,
insects and fungi. Associated compositional, structural and functional
changes of forests will affect almost all goods and services provided
by forests including secondary effects on income of forest owners
and on the climate change mitigation potential of these forests.

In Central and Eastern Europe Norway spruce is currently facing
unprecedented severe threats by multiple abiotic and biotic stressors.
The current forest health crisis points to the low resilience and
adaptability of Norway spruce to climate change, and underpins its
vulnerability to climate warming, hence, giving reason to question the
size of the climate change mitigation potential of the future forests.

Mid-term climate-sensitive scenarios will be developed and will provide
forecasts of spruce wood supply, with particular emphasis on the transition to
a carbon-neutral, green economy. The expected results will be unique and timely
information on future availability of Norway spruce wood (particularly timber).
This information will be of high relevance for the forestry-wood sector and for
the policy development towards transition to green economy in Europe.

### Adaptation of project aims
Due to the Russian war of aggression against Ukraine, we unfortunately had to
adjust our project goals. Due to the suspension of the Russian partner
institution, we will focus on the remaining partner regions (Western Ukraine and
Southwestern Germany) in the future. During the first project year, the project
area of Northwest Russia was also considered. The focus was on the past
development and current status of spruce ([work package 1](../research)).

The further project results will therefore not be able to draw a comparison
between the future of temperate forests of Western Ukraine and Southwestern
Germany and the development of the boreal forests of Northwestern Russia.

Date: 17.08.2022
