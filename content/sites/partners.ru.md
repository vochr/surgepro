---
title: "Партнеры"
description: "ALU - UNFU - FVA"
slug: "partners"
image:
keywords: "ALU"
categories:
    - ""
    - ""
date: 2017-10-31T22:26:13-05:00
draft: false
---
Сейчас над разработкой проекта работают три исследовательских учреждения с двух стран - Украины и Германии.
Существует долгая история сотрудничества между этими институтами и исследователями. Что касается SURGE-Pro, то по этой теме было проведено короткое вводное пилотное исследование под названием **SURGE**. Окончательный отчет об этом предварительном исследовании можно найти [здесь]({{< relref "results.md#SURGE" >}}).

---

![../../../logo/IWW.jpg](../../../logo/IWW.jpg)\
[Кафедра роста леса и дендроэкологии](http://www.iww.uni-freiburg.de/chair-of-forest-growth?set_language=en)  
Факультет окружающей среды и природных ресурсов  
AУниверситет Альберта-Людвига во Фрайбурге, Германия  

---

![../../../logo/UNFU.jpg](../../../logo/UNFU.jpg)\
[Национальный лесотехнический университет Украины, Львов, Украина (НЛТУ)](https://nltu.edu.ua/index.php/en/ )

---

![../../../logo/fva.jpg](../../../logo/fva.jpg)\
Отдел биометрии и информационных наук  
[Институт лесных исследований Баден Вюртемберг, Фрайбург, Германия](http://www.fva-bw.de)  
