---
title: "Funding of the project"
description: ""
slug: "funding"
image: funding_waterfall.jpg
keywords: ""
categories:
    - ""
    - ""
date: 2017-10-31T21:28:43-05:00
draft: false
---

![../../../logo/VWST_W.png](../../../logo/VWST_W.png)

SURGE-Pro, as well as its predecessor SURGE, is generously funded by
the [VolkswagenStiftung](https://www.volkswagenstiftung.de/en)
(Az: 97781).\
The VolkswagenStiftung is the largest German private nonprofit
organization for the promotional of research and education in the
sciences, social sciences, and humanities. It is *not* affiliated to
the Volkswagen Group, economically independent and autonomous in their
decisions.
