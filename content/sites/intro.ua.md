---
title: "Про проєкт"
description: ""
slug: "intro"
image: intro_smallspruce.jpg
keywords: ""
categories:
    - ""
    - ""
date: 2017-10-31T21:28:43-05:00
draft: false
---

На протязі останніх кількох століть ялина європейська (_Picea abies_ [L.] Karst.) широко культивувалася
у багатьох країнах Центральної, Північної та Східної Єпропи. Ці деревостани добре відновлювалися,
характеризувалися швидким ростом та мали значний економічний ефект. Сьогодні поширення
таких деревостанів більше обмежене системою ведення лісового господарства в них,
ніж природними чинниками. У Північно-Західній Росії ялинові деревостани займають
переважно природні ареали зростання, тоді як у Західній Україні та більшій мірі у
Південно-Західній Німеччині вони зростають далеко за межами природних ареалів.
Склад ялинових деревостанів значно змінюється під впливом інтенсивних кліматичних
змін та значно залежить від системи ведення лісового господарства. Ці зміни будуть мати вплив
на якість лісової продукції, поглинання ними вуглецю, кругообіг поживних речовин,
стійкість ялинових деревостанів до природних чинників, зокрема вітростійкість,
пожежостійкість, стійкість до комах та шкідників, грибів. Структурні та
функціональні зміни лісів будуть впливати на всі пов'язані товари та послуги,
які отримують з лісу, зокрема на доходи власників таких лісів, можливості
деревостанів пом'якшувати негативні наслідки змін клімату.

На сьогоднішній день ялинові деревостани у Центральній та Східній Європі
стикаються із надзвичайно серйозними викликами біотичного та абіотичного характеру.
Існуючий санітарний стан ялинових деревостанів вказує на низьку їх стійкість
та пристосовуваність до кліматичних змін, зумовлюючи зростання їх вразливості через підвищення
температур. Це дає підстави для песимістичної оцінки потенціалу майбутніх
ялинових деревостанів та їх здатності пом'якшувати вплив змін клімату.  

У даному проєкті передбачається розробити кліматичні сценарії, які дадуть прогнозну інформацію
щодо деревини ялини європейської, де основний акцент зроблено на переході до "вуглецево-нейтральної
економіки". Дана інформація матиме значний вплив на галузь лісового господарства та для переходу
до зеленої економіки у Європі.

### Адаптація цілей проєкту
Через загарбницьку війну Росії проти України нам, на жаль, довелося скоригувати цілі нашого проекту.
У зв’язку з призупиненням діяльності російської установи-партнера у подальших дослідженнях, ми зосередимося
на дослідженнях території Західної України та Південно-Західної Німеччини. У перший проектний рік дослідження також
проводилися у Північно-Західній Росії. Основна увага була зосереджена на минулому розвитку та поточному стані ялини
європейської ([Робочий пакет 1](../research/)). 

Таким чином, результати проєкту не зможуть бути порівняні у Західній Україні та Південно-Західної Німеччини
з розвитком бореальних лісів у Північно-Західній Росії.

Оновлено: 17.08.2022
