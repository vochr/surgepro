---
title: "(ursprünglich geplantes) Forschungsprogramm"
description: "Projektbeschreibung"
slug: "researchplanned"
image: researchplan.jpg
keywords: ""
categories:
    - ""
    - ""
date: 2017-10-31T22:42:51-05:00
draft: false
---

**Diese Seite ist veraltet und bezieht sich auf den Zeitraum Februar 2021 bis
Februar 2022! Aufgrund des russischen Angriffskriegs gegen die Ukraine und die
Suspendierung des russischen Partners musste der Arbeitsplan angepasst werden.\
Der aktuelle Stand des Arbeitsplans findet sich [hier](../research)**.\
Stand: 03.05.2021

## Ziele
Das übergeordnete Projektziele ist eine konsistente Abschätzung der zukünftigen
Rolle der Fichte in den drei Modellregionen. Um dieses Ziel zu erreichen stellen
wir folgende Hypothesen in den Raum:

1. die aktuelle Entwicklung an Wachstum, Produktivität, Mortalität und
Verjüngung in der Fichte (_Picea abies_) in der Westukraine und
Südwestdeutschland indizieren eine **nicht-nachhaltige** Entwicklung.
2. die Wachstums- und Produktivitätssteigerungen der Fichte in den
borealen Wäldern Nordwestrusslands können teilweise die Verluste in
anderen Regionen kompensieren.

Der dazu ausgearbeitete Forschungs- und Arbeitsplan orientiert sich an
drei Datensträngen:
**Waldinventurdaten**, **Waldwachstumsdaten**, and
**Fernerkundungsdaten** (vgl. Abbildung oben). Diese drei Datenherkünfte
werden zur Nutzung im Waldwachsstumssimulator **EFISCEN** mit einander
verbunden und kombiniert. **EFISCEN** ist ein am
[European Forest Institute (EFI)](https://efi.int/) entwickelter
Waldwachstumssimulator.

## Arbeitspakete (WP)

Das Projekt ist thematisch in verschiedene Arbeitspakete aufgeteilt.
Neben der Administration (WP 0), werden die Aufgaben innerhalb von sieben
Arbeitspakten mit verschiedenen Schwerpunkten und Verantwortlichkeiten
bearbeitet.

### WP 1: Forstinventurdaten
Erfassung und Erhebung von Inventurdaten zu Fichtenwäldern in den
gemeinsam definierten Untersuchungsregionen der Westukraine (Lviv,
Ivano-Frankivs'k, Chernivtsi, Sakarpatska), Nordwestrusslands
(Leningrad, Novgorod, Pskov) und Südwestdeutschlands
(Baden-Württemberg). Darunter fallen Informationen zu Fläche, Vorrat,
Produktivität, Verteilung dieser Daten nach Altersklassen (z.B.,
jung, mittel, alt, hiebsreif and überaltert), Zuwachs
(Gesamtzuwachs, mittlerer Zuwachs), Hiebssatz und realisierter Einschlag
und ebenso Informationen zur Mortalität. Datenquellen werden
hauptsächlich die offiziellen Forststatistiken sein um ein hohe Maß an
Datenqualität und -herkunft sichern zu können. Diese Daten werden die
Basis für die Waldwachstumsmodellierung und -projektion der
Fichtenwälder mit EFISCEN sein.

Ergebnis des Arbeitspakets ist eine diese Daten enthaltende Datenbank.

Kontakt: SPSFTU

### WP 2: Waldwachstumsdaten
Das Arbeitspaket 2 verantwortet die Auswahl von Probeflächen zur
Gewinnung von Bohrkernen für die Zuwachsbestimmung. Es erfolgt eine
Differenzierung nach Altersklassen (< 40 Jahre,
41 - 80 Jahre, mehr als 80 Jahre), Höhenlage, Geländeneigung,
Bestandestypen sowie geographischem Großraum für die drei Modellregionen
Westukraine, Nordwestrussland und Südwestdeutschland.

Zuwachsbohrkerne werden an Probebäumen entnommen und in den jeweiligen
Instituten werden die Jahrringbreiten abgeleitet. Es folgen
Qualitätsprüfungen und eine Datierung entsprechend der Regeln und
Prinzipien, die in dendroökologischen sowie Zuwachs- und Ertragsstudien
angewandt werden.

Als Ergebnis steht eine gemeinsame Zuwachsdatenbank, die die gewonnenen
und harmonisierten Daten enthält.

Kontakt: UNFU

### WP 3: Satellitendaten
In diesem Arbeitspaket werden Satellitendaten verschiedener Sensoren
(MODIS, Landsat und Sentinel-2) für die drei Untersuchungsgebiete
Westukraine, Nordwestrussland und Südwestdeutschland akquiriert. Es
werden alle verfügbaren Daten (Landsat ab ca. 1980, MODIS ab dem Jahr
2000 und Sentinel-2 ab 2015) ohne Wolkenbedeckung (kleiner 10%) in
5-Jahres-Schritten genutzt. Die Daten werden vorprozessiert
(Atmosphärenkorrektur, etc. ) und in einer Datenbank abgelegt.

Des weiteren wird der NDVI abgeleitet um Zusammenhänge zu den
Wachstumsdaten zu analysieren. Für alle Modellregionen wird die Dynamik
der Waldbedeckung (Nadelholz) für 5-Jahres-Schritten prognostiziert.

Als Ergebnis steht eine Bilddatenbank und eine Prognose zur
Waldbedeckung.

Kontakt: UNFU

### WP 4: GIS-Daten
Definition und Entwicklung einer spezifisch für den Zweck ausgerichteten
GIS-Datenbank um die Fernerkundungsdaten und die Ergebnisse der
georeferenzierten Jahrringanalysen aller drei Untersuchungsgebiete
(Westukraine, Nordwestrussland und Südwestdeutschland) miteinander zu
verschneiden. Abgezielt wird auf eine retrospektive Analyse des
Baumwachstums der Fichte (_Picea abies_) auf einem Nord-Süd-Gradienten
für Nordwestrussland und in Bezug zur Höhenlage unter Berücksichtigung
des Temperaturgradienten für die Westukraine und Südwestdeutschland.

Als Ergebnis steht eine GIS-Datenbank sowie die Analyse des
Fichtenwachstums in unterschiedlichen Gradienten.

Kontakt: SPSFTU

### WP 5: Waldwachstumssimulation
Das Waldwachstumsmodell EFISCEN wurde bisher noch nicht genutzt um die
Waldentwicklung und das Holzaufkommen in Baden-Württemberg auf Basis der
Bundeswaldinventurdaten (BWI) auszuwerten. In diesem Arbeitspakt werden
die aktuell (Stand: 2021)
neuesten Inventurdaten für diesen Zweck genutzt. Auch die in vorherigen
Arbeitspaketen zusammengestellten Basisdaten für die Westukraine und
Nordwestrussland werden für diesen Zweck aufbereitet und mit EFISCEN
untersucht. Als wesentlicher Schritt werden aus den gewonnenen Daten der
anderen Arbeitspakete (insbesondere AP 2) Wachstumsfunktionen für die
Fichte angepasst und in EFISCEN eingebaut. Daneben werden gemeinsame
Szenarios für EFISCEN entworfen. Die EFISCEN-Ergebnisse für
Baden-Württemberg werden mit den Prognosen des WEHAM-Modells verglichen,
die standardmäßig zur Auswertung und Prognose der BWI-Daten genutzt
werden.

Die Ergebnisse für alle drei Regionen werden gemeinsam analysiert und
interpretiert.

Kontakt: FVA BW

### WP 6: Synthese
Aus Basis der Ergebnisse der Simulationsstudien werden die Konsequenzen
der verschiedenen Szenarien auf die Ökosystemdienstleistungen der Wälder
untersucht und diskutiert. Es werden publizierte Zusammenhänge zwischen
strukturellen Ökosysteminformationen (welche das Ergebnis der
Modellierung darstellen) und Ökosystemdienstleistungen genutzt um eine
zusammenfassende Bewertung vornehmen zu können.

Als Ergebnis steht ein Bericht, der alle wesentlichen methodischen
Schritte und inhaltlichen Ergebnisse die zu der erzielten Bewertung
geführt haben, darstellen.

Kontakt: ALU

### WP 7: Ergebnisverwertung
Die geplanten Aktivitäten zur Ergebnisverwertung zielen auf einen
Transfer des neu generierten Wissenstands zur Unterstützung der
Entscheidungsträger_innen auf verschiedenen Handlungsebenen der
Raumnutzungsplanung.

Dies soll erreicht werden durch die aktive Einbindung verschiedener
Akteure bei der Weitergabe und Verbreitung der Projektergebnisse:
1. Vorbereitung von wissenschaftlichem Material und Publikationen auf
Basis der Daten und Ideen, die während der Projektlaufzeit generiert
wurden.
2. Erarbeitung von Unterrichtsmaterial für Studenten auf Bachelor-,
Master- und PhD-Niveau, wie zum Beispiel thematische Einheiten für
Module zur nachhaltigen Forstwirtschaft.
3. Nutzung der Projektergebnisse zur Aktualisierung von
Unterrichtsinhalten in Ausbildungsinstitutionen.
4. Darstellung der Projektergebnisse auf Konferenzen, in Seminare,
Workshops, Summer Schools u.ä., aber auch in Massenmedien und für
interessierte Zielgruppen wie Forstämter, Revierleitende, Repräsentanten
des Forstsektors, NGOs des Naturschutzes und weitere.
5. durch die (Weiter)Entwicklung dieser Projektwebseite und thematischen
Broschüren.

Kontakt: UNFU and SPSFTU
