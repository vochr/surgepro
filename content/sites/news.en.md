---
title: "Latest news from the SURGE-Pro project"
description: ""
slug: "news"
image:
keywords: ""
categories:
    - ""
    - ""
date: 2017-10-31T21:28:43-05:00
draft: false
---

Date: 2023-07-20\
The Ukrainian scientists working with Prof. Dr. Vasyl Lavnyy gave presentations
on their work with a focus on the SURGE-Pro project during their research stay
at the University of Freiburg in July 2023. The slides of the presentations can
be found under [results](../results) or directly [here.](../seminar)

---
Date: 2022-10-06 08:33\
SURGE-Pro participated in the all-IUFRO conference in Vienna from 21-23.09.2022.
The project was introduced with a poster and first results were reported. The
poster can also be found on this website in the [results section](../results).

---
Date: 2022-07-19 17:21\
The Russian war of aggression on Ukraine has also affected our project. In
accordance with the requirements of our project funder, the Volkswagen
Foundation, the Russian project partners were suspended on
February 25, 2022 until further notice.

While an early and peaceful end of the war is not expected within the remaining
project duration (until Jan. 2024), the project is now definitely running as
a bilateral research project. The Russian colleagues have kindly agreed to a
reallocation of funds towards out Ukrainian researcher.

---
Date: 2022-02-25 11:04\
The Russian invasion of Ukraine has surprised, shocked and deeply concerned all
project staff. Our thoughts are with our Ukrainian colleagues. The cooperation
with Russian institutions will be frozen until further notice.

---
