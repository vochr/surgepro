---
title: "Project Partners"
description: "ALU - UNFU - STSFTU - FVA"
slug: "partners"
image: partners_Herderbau.png
keywords: "ALU"
categories:
    - ""
    - ""
date: 2017-10-31T22:26:13-05:00
draft: false
---
Currently, three research institutes from two countries (Ukraine and Germany)
are involved in this project.
There is a long history of institutional and personal collaboration between those
institutes and its members. With respect to
SURGE-Pro, there was a short introductory pilot study on the
topic, called **SURGE**. The final report on this preliminary study can
be found [here]({{< relref "results.md#SURGE" >}}).

---

![../../../logo/IWW.jpg](../../../logo/IWW.jpg)\
[Chair of Forest Growth and Dendroecology](http://www.iww.uni-freiburg.de/chair-of-forest-growth?set_language=en)  
[Fakultät for Umwelt und natürliche Ressourcen](https://www.unr.uni-freiburg.de/en)\
[Albert-Ludwig-Universität Freiburg](https://www.uni-freiburg.de/en)

---

![../../../logo/UNFU.jpg](../../../logo/UNFU.jpg)\
[Ukrainian National Forestry University (UNFU), Lviv, Ukraine](https://nltu.edu.ua/index.php/en/ )

---

<!-- ![../../../logo/UNFU.jpg](../../../logo/STSFTU.jpg)\
[St. Petersburg State Forest Technical University, St. Petersburg, Russia](http://en.spbftu.ru/) -->


![../../logo/fva.jpg](../../logo/fva.jpg)\
Department of Biometry and Information Sciences  
[Forest Research Institute Baden Württemberg, Freiburg, Germany](http://www.fva-bw.de)  

---

At the beginning of the project, there was a fourth partner institution
(St. Petersburg State Forest Technical University).
It has been suspended from project tasks since 25th of February 2022 due to the
Russian war of against Ukraine. See also our [News section]({{< relref "news.md" >}}).
