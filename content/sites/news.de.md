---
title: "Neuigkeiten aus dem SURGE-Pro Projekt"
description: ""
slug: "news"
image:
keywords: ""
categories:
    - ""
    - ""
date: 2017-10-31T21:28:43-05:00
draft: false
---

Datum: 2023-07-20\
Die ukrainischen Wissenschaftler um Prof. Dr. Vasyl Lavnyy haben am 20.07.2023
im Zuge ihres Forschungsaufenthalts an der Universität Freiburg im July 2023
Vorträge über ihre Arbeit mit einem Fokus auf das SURGE-Pro Projekt gehalten.
Die Folien der Vorträge finden sich unter [Ergebnisse](../results) oder direkt
[hier.](../seminar)

---
Datum: 2023-07-10\
Die Projektpartner von SURGE-Pro arbeiten an sehr verschiedenen Orten. Umso
schöner war es, alle zum ersten Mal beim SURGE-Pro Workshop in Freiburg
begrüßen zu können. Alle vorherigen geplanten Treffen bei den
Partneruniversitäten konnten aufgrund der Covid-Pandemie und dem russischen
Angriffskrieg nicht durchgeführt werden.

---
Datum: 2022-10-06 08:33\
SURGE-Pro hat an der
[all-IUFRO Konferenz](https://www.iufro.org/events/all-iufro-conference-2022/)
in Wien vom 21-23.09.2022 teilgenommen.
Das Projekt wurde mit einem Poster vorgestellt und erste Ergebnisse
kommuniziert. Das Poster finden sie auch auf dieser Webseite im Bereich
[Ergebnisse](../results).

---
Datum: 2022-07-19 17:21\
Der russischen Angriffskriegs auf die Ukraine hat auch unser Projekt in
Mitleidenschaft gezogen. Entsprechend der Vorgaben unseres Projektträgers
VolkswagenStiftung wurden die russischen Projektpartner am 25.02.2022 bis auf
Weiteres suspendiert.

Während ein baldiges friedliches Ende des Kriegs innerhalb der restlichen
Projektlaufzeit (bis Jan. 2024) nicht absehbar ist, läuft das
Projekt nun definitiv als bilaterales Forschungsprojekt weiter.
Freundlicherweise haben die russischen Kolleg:innen einer Mittelumwidmung
zugunsten der ukrainischen Wissenschaftler:innen zugestimmt.

---
Datum: 2022-02-25 11:04\
Die russische Invasion in der Ukraine hat alle Projekt-Mitarbeitenden
überrascht, geschockt und zutiefst beunruhigt. Unsere Gedanken sind bei unseren
ukrainischen Kolleg:innen. Die Zusammenarbeit mit russischen Institutionen
werden bis auf weiteres eingefroren.

---
